fpm -t rpm -x argos.sh -x changelog --rpm-changelog changelog \
-x post.sh -x postun.sh -x .git --license Proprietary --url https://apisnetworks.com \
--config-files=etc/monit.d/00-argos.conf --config-files=root/.mailfilter --after-install post.sh \
--after-remove postun.sh -a noarch -d monit -d python2-pip --rpm-use-file-permissions -s dir \
-m '<build@apisnetworks.com>' --epoch 1 -v 1.0  --iteration 18.1.apnscp -n argos .
