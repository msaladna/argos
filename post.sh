#!/bin/sh
NTFY_VERSION=2.6.0
PIPBIN=/usr/bin/pip
[[ ! -f $PIPBIN ]] && PIPBIN=/usr/bin/pip3
useradd -s /bin/false -r monit -c "Argos monitoring agent" -M -d /dev/null 2> /dev/null || true
grep -q "mailserver" /etc/monit.d/00-argos.conf
R1=$?
grep -q "monit" /etc/passwd
R2=$?
if [[ $R1 -ne 0 || $R2 -ne 0 ]]; then 
  sed -i -e 's!^.*username monit password.*$!!g' /etc/monit.d/00-argos.conf
  PASSWORD=x$(openssl rand -base64 32)
  echo $PASSWORD | passwd --stdin monit
  echo "set mailserver localhost username monit password \"$PASSWORD\"" >> /etc/monit.d/00-argos.conf
  test ! `grep -q monit /etc/smtp.pamlist` && echo monit >> /etc/smtp.pamlist
fi
# Prefer .conf to avoid obliterating or duplicate loading .rpmsave configs
grep -q "^\s*include /etc/monit\.d/\*$" /etc/monitrc
[[ $? -eq 0 ]] && sed -i -e 's!^\s*include /etc/monit\.d/\*$!!g' /etc/monitrc
grep -q "^\s*include /etc/monit\.d/\*\.conf$" /etc/monitrc
[[ $? -ne 0 ]] && echo -e "\ninclude /etc/monit.d/*.conf" >> /etc/monitrc

read -r -d '' BLOCK <<'EOF'
### BEGIN ARGOS BLOCK ###
if (/From: monit@localhost/)
{
        cc "|ruby $HOME/notify.rb"
        to $DEFAULT
}
### END ARGOS BLOCK ###
EOF

grep -q 'notify.rb' /root/.mailfilter 

if test $? -ne 0 ; then 
	echo "$BLOCK" >> /root/.mailfilter
fi

# no -q, pip doesn't handle SIGPIPE
$PIPBIN show ntfy | grep -F $NTFY_VERSION > /dev/null
if test $? -ne 0 ; then
	$PIPBIN install -U ntfy==$NTFY_VERSION
fi

# Ensure password is always restricted
[[ -f /etc/monit.d/00-argos.conf ]] && chmod 600 /etc/monit.d/00-argos.conf
for i in /etc/monit.d/*.conf ; do 
  [[ -f $i.disabled ]] && mv $i $i.disabled
done

/usr/bin/systemctl try-restart monit
